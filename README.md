# generator-stuff-component

Quickly scaffold a component repository using [stuff-build-tools]()

## Installation

First, install [Yeoman](http://yeoman.io), [npm](https://www.npmjs.com/) and [node.js](https://nodejs.org/).

```bash
$ npm install -g yo
```

Clone a local copy of the generator and create a symlink

```bash
$ git clone git@bitbucket.org:fairfax/generator-stuff-component.git
$ cd generator-stuff-component
$ npm link
```

Then generate your new project:

```bash
$ cd ..
$ yo stuff-component
```
