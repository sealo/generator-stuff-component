'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');

module.exports = yeoman.Base.extend({
  prompting: function () {
    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the fine ' + chalk.red('yo stuff-component') + ' generator!'
    ));

    var prompts = [{
      type: 'input',
      name: 'componentName',
      message: 'What is your component name?',
      default: 'ffx-sample-component' + Math.floor(Math.random() * 10000)
    }];

    return this.prompt(prompts).then(function (props) {
      // To access props later use this.props.someAnswer;
      this.props = props;
    }.bind(this));
  },

  writing: function () {
    this.fs.copy(
      this.templatePath('component'),
      this.destinationPath(this.props.componentName)
    );

    this.fs.copyTpl(
      this.templatePath('component/package.json'),
      this.destinationPath(this.props.componentName + '/package.json'),
      { componentName: this.props.componentName }
    );
  },

  install: function () {}
});
